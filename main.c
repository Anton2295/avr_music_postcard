/*
 * 8_bit_music V2.c
 *
 * Created: 11.09.2018 22:15:39
 * Author : Антон
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>

#include "notes.h"
#include "music.h"

 typedef enum  {Pulse, Triange, Noise,DPCM} typeChanal;

	 


	
#define  zeroValue 0


//--------------Общие переменные-----------

unsigned char namberRow  = 0;

unsigned char nomberFrams = 0  ;

#define VolumeX  16


//------------------------------------


struct Channel
{
	
	const  typeChanal typec;
	
	unsigned char amplitude;
	
	unsigned char note;	
	
	unsigned char dutyRatio ;
	
	unsigned char arpeggio;
	

	unsigned char valueNamber;
	
	unsigned char ArpeggioNamber;
	
	unsigned char dutyNamber;
	
	unsigned short  waveCount;
	
	char isUp ; // для треугольника
	
};




#define quantityChannel 1

#define TRUE 1
#define FALSE 0

struct Channel channels[] = {{Pulse},{Pulse}};
	

unsigned char getValue(unsigned char namberChannel);

void setValue(char value);

void nextTick();

//void setNote(char nomberChannel,char note, unsigned char amplitude, unsigned char dutyRatio );

#define    tickInNote   1042// 17 ms /  0,032 ms почему на 2 !?!?!?!?!?!?

short tickCout = 0;

void nextNote(unsigned char channelNamber);

short noteCout = 0;





void  initPwm()
{
// Timer/Counter 0 initialization
// Clock source: System Clock
// Clock value: 8000,000 kHz
// Mode: Fast PWM top=0xFF
// OC0A output: Non-Inverted PWM
// OC0B output: Disconnected
// Timer Period: 0,032 ms
// Output Pulse(s):
// OC0A Period: 0,032 ms Width: 0,016063 ms
TCCR0A=(1<<COM0A1) | (0<<COM0A0) | (1<<COM0B1) | (1<<COM0B0) | (1<<WGM01) | (1<<WGM00);
TCCR0B=(0<<WGM02) | (0<<CS02) | (0<<CS01) | (1<<CS00);
TCNT0=0x00;
OCR0A=0x80;
OCR0B=0x00;
	
	DDRD=(1<<DDD6) |  (1<<DDD5) ;
	
	DDRC=(1<<DDC6); // контрольный сигнал
	
	// Timer/Counter 0 Interrupt(s) initialization
	TIMSK0=(0<<OCIE0B) | (1<<OCIE0A) | (0<<TOIE0);
	
	sei();
}

 ISR(TIMER0_COMPA_vect)
 {
	   nextTick();
	   
	   tickCout++;
	   
	   if (tickCout >= tickInNote)
	   {
		   tickCout = 0;
		   
		   for(unsigned char i = 0; i < quantityChannel;i++)
		   {
			   nextNote(i);
		   }
		   
		   
		   noteCout++;
		   
		   // каждые 17 мс
		   
		   if(noteCout >= speed )
		   {
			   noteCout = 0;
			   			   
			   if(namberRow < quantityRows - 1)
					 namberRow++;
				else
				{
					if(nomberFrams < quantitynomberFrams - 1)
					{
						namberRow = 0;
						nomberFrams++;
					}	
				}
		   }
		   
		   		   			 		 
	   }
	   
 }
 
 void nextTick()
 {
	unsigned char sum = 0;
	
	for(unsigned char i = 0; i < quantityChannel;i++)
	{
		channels[i].waveCount++;	
		sum +=  getValue(i);
	}
	
	setValue(sum);
 }
 
 

void nextNote(unsigned char channelNamber )
{
	struct Channel * channel = &channels[channelNamber];
	struct Row row = getRow(namberRow,nomberFrams,channelNamber);
	

	//if (row.note != channel->note && row.note != _)
	if (row.note != _)
	{
		if (row.note != STP)
		{
			channel->valueNamber = 0;
			channel->ArpeggioNamber = 0;
			channel->dutyNamber = 0;
			
			channel ->note = row.note;

		}



	}
	
		static struct  Instrument instrument;
		
		
		 if (row.namberInstrumens != _ )
		 {

				 instrument = instruments[row.namberInstrumens];			  
		 }

		
		if(instrument.Volume ->quantity > 0)
		{
				
		unsigned char volume = instrument.Volume->values[channel->valueNamber];
		
		channel->amplitude = volume;
				
		if(channel->valueNamber < instrument.Volume->quantity - 1)
		{
			if(channel->valueNamber != instrument.Volume->release || row.note == STP)
				channel->valueNamber++;
		}
		else
		 {
			 if (instrument.Volume->loop != NO)
				channel->valueNamber = instrument.Volume->loop;
		 }
		
		}
		else
		channel->amplitude = 16 * VolumeX/2 - 1;
		
		
		if(instrument.Arpeggio ->quantity > 0)
		{
					
			unsigned char arpeggio = instrument.Arpeggio->values[channel->ArpeggioNamber];
					
			channel->arpeggio = arpeggio;
			

															
			if(channel->ArpeggioNamber < instrument.Arpeggio->quantity - 1 )
			{
				if(channel->ArpeggioNamber != instrument.Arpeggio->release || row.note == STP)
					channel->ArpeggioNamber ++;
			}
			else
			{
				if (instrument.Arpeggio->loop != NO)
				channel->ArpeggioNamber = instrument.Arpeggio->loop;
			}
								
		}
		else
		channel->arpeggio = 0;
		
		if(instrument.Duty ->quantity > 0)
		{
					
			unsigned char duty = instrument.Duty->values[channel->dutyNamber];
					
			channel->dutyRatio = duty;
					
			if(channel->dutyNamber < instrument.Duty->quantity - 1 )
			{
				if(channel->dutyNamber != instrument.Duty->release ||row.note == STP)
					channel->dutyNamber++;
			}
			else
			{
				if (instrument.Duty->loop != NO)
				channel->dutyNamber = instrument.Duty->loop;
			}
		}
		else
		channel->dutyRatio = 0;
		
	

}
	
	


 void setValue( char Value)
 {
	OCR0A = Value;
	OCR0B = Value;
 }
 

 
 
 unsigned char getValue(unsigned char namberChannel)
{
	

struct Channel *channel = &channels[namberChannel] ;

	if(channel ->note == zero)
	return zeroValue;
	

if (channel->typec == Pulse)
{
	 char dutyRatio = 0;
	 
	 switch(channel->dutyRatio){
		 case (0):dutyRatio = 32; break;
		 case (1):dutyRatio = 64; break;
		 case (2):dutyRatio = 128; break;
		 case (3):dutyRatio = 192; break;
	 }
	 
	 unsigned short   swichCoutValue = notes[channel->note + channel->arpeggio] * dutyRatio / 255 ;
	 
	 if(channel-> waveCount <  swichCoutValue)
	 return zeroValue;
	 else
	 {
		 if(channel->waveCount >= notes[channel->note + channel->arpeggio] )
		 channel->waveCount = 0;
		 
		 
		 return  (zeroValue + channel->amplitude) * VolumeX;
	 }
} 
else
{
	if (channel->typec == Triange)
	{
		// реализовать трехугольник
		char triangeAmplitude;
		
		if(channel->amplitude > 0)		
		triangeAmplitude  = 16 * VolumeX - 1;
		else
		triangeAmplitude  = 0;
		
		
		if(channel->waveCount >= notes[channel->note + channel->arpeggio]/2)
		{
			channel->waveCount = 0;
			
			if (channel->isUp == TRUE) 
				channel->isUp = FALSE;
			else
				channel->isUp = TRUE;
		}
		
		
		if (channel->isUp == TRUE)
		{
			
			return 2*triangeAmplitude * channel->waveCount /( (notes[channel->note + channel->arpeggio]/2 ));
		} 
		else
		{
				
			return  2*triangeAmplitude - 2*triangeAmplitude * channel->waveCount / ((notes[channel->note + channel->arpeggio]/2)) ;
		}
				
				
	}
	else
		if (channel->typec == Noise)
		{
				 char dutyRatio = 0;
				 
				 switch(channel->dutyRatio){
					 case (0):dutyRatio = 32; break;
					 case (1):dutyRatio = 64; break;
					 case (2):dutyRatio = 128; break;
					 case (3):dutyRatio = 192; break;
				 }
				 
				 unsigned short   swichCoutValue = notes[channel->note + channel->arpeggio] * dutyRatio / 255 ;
				 
				 if(channel-> waveCount <  swichCoutValue)
				 return zeroValue;
				 else
				 {
					 if(channel->waveCount >= notes[channel->note + channel->arpeggio] )
					 channel->waveCount = 0;
					 
					 
					 return  (zeroValue + channel->amplitude) * VolumeX;
				 }
		}
}

return zeroValue;

 }
 
//  
//  void setNote(unsigned char nomberChannel,char note, unsigned char amplitude, unsigned char dutyRatio )
//  {
// 	 if (note != _)
// 	 {
// 		 channels[nomberChannel].note = note;		 
// 	 }
// 	 
// 	 
// 	 if (channels[nomberChannel].amplitude != _)
// 	 {
// 		 channels[nomberChannel].amplitude = amplitude; 
// 	 }
// 	 
// 	 
//    	if (channels[nomberChannel].dutyRatio != _)
// 	{
// 		channels[nomberChannel].dutyRatio = dutyRatio;
// 	}
//  }
 

 
int main(void)
{
	// Declare your local variables here

	// Crystal Oscillator division factor: 1
	CLKPR=(1<<CLKPCE);
    CLKPR=(0<<CLKPCE) | (0<<CLKPS3) | (0<<CLKPS2) | (0<<CLKPS1) | (0<<CLKPS0);
	
	initPwm();
	
    /* Replace with your application code */
	//setNote(&channels[0],A3,64,1);
	


    while (1) 
    {
    }
}

