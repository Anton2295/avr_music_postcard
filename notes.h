﻿/*
 * notes.h
 *
 * Created: 11.09.2018 22:17:25
 *  Author: Антон
 */ 


#ifndef NOTES_H_
#define NOTES_H_

#define mainFr 16000000/256 

#define zeroNote  41

#define zerofr  0
#define zero    41 - zeroNote

#define B4fr    mainFr * 100 /98775
#define B4      42 - zeroNote
#define A_4fr	mainFr * 100 /93232
#define A_4     43 - zeroNote
#define A4fr	mainFr * 100 /88000
#define A4      44 - zeroNote
#define G_4fr	mainFr * 100 /83060
#define G_4     45 - zeroNote
#define G4fr	mainFr * 100 /78400
#define G4      46 - zeroNote
#define F_4fr	mainFr * 100 /73998
#define F_4     47 - zeroNote
#define F4fr	mainFr * 100 /69846
#define F4      48 - zeroNote
#define E4fr	mainFr * 100 /65926
#define E4      49 - zeroNote
#define D_4fr	mainFr * 100 /62226
#define D_4     50 - zeroNote
#define D4fr	mainFr * 100 /58732
#define D4      51 - zeroNote
#define C_4fr	mainFr * 100 /55436
#define C_4     52 - zeroNote
#define C4fr	mainFr * 100 /52325
#define C4      53 - zeroNote

#define B3fr    mainFr * 100 /49388
#define B3      54 - zeroNote
#define A_3fr	mainFr * 100 /46616
#define A_3     55 - zeroNote
#define A3fr	mainFr * 100 /44000
#define A3      56 - zeroNote
#define G_3fr	mainFr * 100 /41530
#define G_3     57 - zeroNote
#define G3fr	mainFr * 100 /39200
#define G3      58 - zeroNote
#define F_3fr	mainFr * 100 /36999
#define F_3     59 - zeroNote
#define F3fr	mainFr * 100 /34923
#define F3      60 - zeroNote
#define E3fr	mainFr * 100 /32963
#define E3      61 - zeroNote
#define D_3fr	mainFr * 100 /31113
#define D_3     62 - zeroNote
#define D3fr	mainFr * 100 /29366
#define D3      63 - zeroNote
#define C_3fr	mainFr * 100 /27718
#define C_3     64 - zeroNote
#define C3fr	mainFr * 100 /26163
#define C3      65 - zeroNote


#define B2fr    mainFr * 100 /24696
#define B2      66 - zeroNote
#define A_2fr	mainFr * 100 /23308
#define A_2     67 - zeroNote
#define A2fr	mainFr * 100 /22000
#define A2      68 - zeroNote
#define G_2fr	mainFr * 100 /20700
#define G_2     69 - zeroNote
#define G2fr	mainFr * 100 /19600
#define G2      70 - zeroNote
#define F_2fr	mainFr * 100 /18500
#define F_2     71 - zeroNote
#define F2fr	mainFr * 100 /17462
#define F2      72 - zeroNote
#define E2fr	mainFr * 100 /16481
#define E2      73 - zeroNote
#define D_2fr	mainFr * 100 /15556
#define D_2     74 - zeroNote
#define D2fr	mainFr * 100 /14783
#define D2      75 - zeroNote
#define C_2fr	mainFr * 100 /13859
#define C_2     76 - zeroNote
#define C2fr	mainFr * 100 /13082
#define C2      77 - zeroNote


#define A      10
#define B     11
#define C      12
#define D      13
#define E      14
#define F      15



const unsigned short notes[] = 
	{
		zerofr, 
		B4fr,A_4fr,A4fr,G_4fr,G4fr,F_4fr,F4fr,E4fr,D4fr,C_4fr,C4fr,
		B3fr,A_3fr,A3fr,G_3fr,G3fr,F_3fr,F3fr,E3fr,D3fr,C_3fr,C3fr,
		B2fr,A_2fr,A2fr,G_2fr,G2fr,F_2fr,F2fr,E2fr,D2fr,C_2fr,C2fr
	};


#endif /* NOTES_H_ */